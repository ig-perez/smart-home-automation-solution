/* eslint-env jest */

const fs = require('fs')
const SmartHomeSource = require('../src/smart-home-source')
const { Config, Record, Result } = require('bespoken-batch-tester')

require('dotenv').config()

describe('load record tests', () => {
  beforeEach(() => {
    Config.reset()
  })

  test('record that is valid', async () => {
    Config.loadFromJSON({
      customer: 'bespoken',
      job: 'test',
      devicesFile: './test/test-devices.csv',
      utterancesFile: './test/test-utterances.csv'
    })

    const source = new SmartHomeSource()
    const records = await source.loadAll()
    expect(records.length).toBe(10)
    const record = records[0]
    expect(record.utterance).toBe('turn on kitchen floor lamp')
    expect(record.meta.device.id).toBe('uuid-for-living-room')
  })
})
