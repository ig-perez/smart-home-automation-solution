const fs = require('fs')
const parse = require('csv-parse/lib/sync')
const { Config, Record, Result, Source } = require('bespoken-batch-tester')

class SmartHomeSource extends Source {
  async loadAll () {
    // Read in the list of devices to test
    const devicesFile = Config.get('devicesFile', [], false, 'input/devices.csv')
    const devices = parse(fs.readFileSync(devicesFile), {
      columns: true
    })

    // Read in the list of utterances to test
    const utterancesFile = Config.get('utterancesFile', [], false, 'input/utterances.csv')
    const utterances = parse(fs.readFileSync(utterancesFile), {
      columns: true
    })

    console.log(`DEVICES ${devices.length}`)

    const records = []
    for (const device of devices) {
      const deviceName = device.name
      for (const utterance of utterances) {
        const utteranceName = utterance.utterance
        const finalUtterance = utteranceName.replace(/\$\{device\}/, deviceName)
        console.log(`SMARTHOMESOURCE LOADALL utterance before: ${utteranceName} after: ${finalUtterance}`)
        const record = new Record(finalUtterance, {},
          {
            device,
            utterance
          })
        records.push(record)
      }
    }
    return records
  }
}

module.exports = SmartHomeSource
